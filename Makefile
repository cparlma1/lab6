SRC_FILES = lab6.c lab6.h Makefile

lab6: lab6.o
	gcc -lpthread lab6.o -o lab6
#	gcc -Wall -lpthread -o lab6 lab6.c -lrt -lm 

lab6.o: lab6.c lab6.h
	gcc -c -w lab6.c -o lab6.o  

#-Wall
clean:
	rm -f lab6 sig *.o

submit: $(SRC_FILES)
	mkdir lab6Parlman_cparlma1
	cp $(SRC_FILES) lab6Parlman_cparlma1
	tar -cvzf lab6Parlman_cparlma1.tar.gz lab6Parlman_cparlma1
	mv lab6Parlman_cparlma1.tar.gz lab6Parlman_cparlma1.tgz
	rm -r lab6Parlman_cparlma1
