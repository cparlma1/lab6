/*
cameron parlman
cs350  lab6

*/


#include <pthread.h>
//#include <stdlib.h>
//#include <stdio.h>
#include "lab6.h"

int main(int argc, char** argv)
{
	srand(time(NULL));
	//validate input 
	char* usage = " usage string ";
	if(argc != 3){
		printf("print usage string\n");
		return 1;
	}

	//setup some variable numThreads and percentage 
	int numThreads;
	numThreads = atoi(argv[1]);		
	percentage = atoi(argv[2]);

	//validate input values
	if(numThreads < 1){
		printf("%s\n",usage);
		return 1;
	}
	else if( numThreads > 1000){
		printf("%s\n",usage);
		return 1;
	}
	else if( percentage < 1){
		printf("%s\n",usage);
		return 1;
	}
	else if(percentage > 20){
		printf("%s\n",usage);
		return 1;
	}
	
	//allocate stocks / numThreads
	//double* stocks;
	stocks = (double*)malloc(sizeof(double) * numThreads); // allocate num of threads
	
	/*fill stocks to default of 100 */
	int i = 0;
	for(i = 0; i < numThreads; i++){
		stocks[i] = 100.0;
	}

	marketValue = 100 * numThreads;
	originalValue = marketValue;

	//set up threads 
	pthread_t watcher[2];
	pthread_t threads[numThreads];

	//finished setup 
	//begin execution

	pthread_create(&watcher[0], NULL, &watchUp, NULL);
	pthread_create(&watcher[1], NULL, &watchDown, NULL);

	//create all threads
	for(i = 0 ; i < numThreads; ++i){
		pthread_create(&threads[i], NULL, &changeStocks, (void*)i);
	}
	
	



	/* wait on threads  */
	int rc;
	rc = pthread_join(watcher[0], NULL); 	
	rc = pthread_join(watcher[1], NULL);

	while(runMarket){};	
	for(i = 0 ; i < 100000; i++){ int b = i%32;}
	printf("Total Market Price of %d Stocks: %.2f\n",numThreads, sumStocks(numThreads));
	/* final ouput */

return 0;
}




/*
   loop and change stocks untill runMarket is false 
 
*/
void* changeStocks(void* stock){
	double random =0;
	while(runMarket){ /* run until watcher executes */
		random =(float)rand() / (float)RAND_MAX*2;
		random = 1 - random;
		
		pthread_mutex_lock(&market);				/*lock on market*/
		while(check == 1)							/*condition*/	
			pthread_cond_wait(&cond, &market);  	/*condition wait on market*/


		//pthread_mutex_lock(&value);	 /*LOCK*/ 		/*lock on value.. unnessesary?*/

			if(!runMarket){							/*if market ended terminate*/	
				pthread_mutex_unlock(&value);		
				check = 1;
				pthread_cond_broadcast(&cond);
				pthread_mutex_unlock(&market);
				pthread_exit(NULL);
			}
			stocks[(int)stock] += random;	 		/*change stock value*/
			marketValue += random;					/*change market value*/

		//pthread_mutex_unlock(&value); /*UNLOCK*/  	/*unlock on value */
	
		check = 1;
		pthread_cond_broadcast(&cond);				/*broadcast signal*/
		pthread_mutex_unlock(&market);				/*unlock market*/

	}	
	pthread_exit(NULL);
	return NULL;
}



//WATCH UPPER LIMIT 
void* watchUp(){
	while(runMarket){
//	printf("up\n");

		pthread_mutex_lock(&market);	
		while(check == 0)
			pthread_cond_wait(&cond, &market); 

		if(marketValue >= originalValue + originalValue*(double)percentage/100){
			printf("Market Up to %.2f\n",marketValue);
			runMarket = 0;
		}

		check = 0;
		pthread_cond_signal(&cond);
		pthread_mutex_unlock(&market);


	}

	printf("up finished\n");
	return NULL;
}




//WATCH LOWER LIMIT
void* watchDown(){
	while(runMarket){
		//	printf("MARKET VALUE %f\n" , marketValue);
//	printf("down\n");

		pthread_mutex_lock(&market);	
		while(check == 0)
			pthread_cond_wait(&cond, &market); 

		if(marketValue <= originalValue - originalValue*(double)percentage/100){
			printf("Market Down to %.2f\n",marketValue);
			//end market	
			runMarket = 0;
		}

		check = 0;
		pthread_cond_signal(&cond);
		pthread_mutex_unlock(&market);

	}
	printf("down finished\n");
	return NULL;
}


double sumStocks(int numStocks){
	double returnSum = 0;
	int i = 0;
	for(i = 0 ; i < numStocks; ++i){
		returnSum += stocks[i];
	}	
	return returnSum;
}
